package OOP;

import java.util.ArrayList;

/**
 * Created by Kristina on 20.01.2017.
 */
public class Korter {
    ArrayList <String> korter = new ArrayList<>();
    int maht;
    int paljuVeelMahub;
    public Korter(int mahutab) {

        maht = mahutab;
    }
    public void saabus(String a) {
        if (korter.size() < maht) {
            korter.add(a);
        }
        else {
            System.out.println("Kohti pole!");
        }
    }
    public void prindiKylalisteArv(){
        System.out.println(korter.size());
    }
    public void prindiPaljuVeelMahub() {
        paljuVeelMahub = maht - korter.size();
        System.out.println(paljuVeelMahub);
    }
    public void lahkus(String a) {
        korter.remove(a);
    }
}
